import React, { useState, useEffect, useContext } from 'react'
import {
  createStackNavigator,
  TransitionPresets
} from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'

import Routes from './Routes'
import SplashScreen from '../screen/splashScreen'
import ListCat from '../screen/listCat'
import DetailCat from '../screen/detailCat'
import Welcome from '../screen/welcome'
import Login from '../screen/login'
import Register from '../screen/register'

import { Context } from '../context'
import actions from '../context/actions'

const { SET_EMAIL } = actions
const Stack = createStackNavigator()

const AuthenticationStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name={Routes.Welcome}
      component={Welcome}
      options={{ headerShown: false, ...TransitionPresets.SlideFromRightIOS }}
    />
    <Stack.Screen
      name={Routes.Login}
      component={Login}
      options={{ headerShown: false, ...TransitionPresets.SlideFromRightIOS }}
    />
    <Stack.Screen
      name={Routes.Register}
      component={Register}
      options={{ headerShown: false, ...TransitionPresets.SlideFromRightIOS }}
    />
  </Stack.Navigator>
)

const HomeStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name={Routes.ListCat}
      component={ListCat}
      options={{ headerShown: false, ...TransitionPresets.SlideFromRightIOS }}
    />
    <Stack.Screen
      name={Routes.DetailCat}
      component={DetailCat}
      options={{ headerShown: false, ...TransitionPresets.SlideFromRightIOS }}
    />
  </Stack.Navigator>
)

const RootStack = createStackNavigator()
const RootStackScreen = ({ isLogin, isLoginContext }) => (
  <RootStack.Navigator headerShown={false}>
    {isLogin || isLoginContext ? (
      <RootStack.Screen
        name="Home"
        component={HomeStack}
        options={{
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS
        }}
      />
    ) : (
      <RootStack.Screen
        name="Auth"
        component={AuthenticationStack}
        options={{
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS
        }}
      />
    )}
  </RootStack.Navigator>
)

const Navigation = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [isLogin, setIsLogin] = useState(false)
  const { state, dispatch } = useContext(Context)

  const getLoginStorage = async () => {
    try {
      const email = await AsyncStorage.getItem('email')
      setTimeout(async () => {
        setIsLogin(email ? true : false)
        dispatch({
          type: SET_EMAIL,
          payload: email
        })
        setIsLoading(false)
      }, 3000)
    } catch (error) {
      setTimeout(async () => {
        await AsyncStorage.setItem('email', null)
        setIsLogin(false)
        setIsLoading(false)
      }, 3000)
    }
  }

  useEffect(() => {
    getLoginStorage()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (isLoading) {
    return <SplashScreen />
  }

  return (
    <NavigationContainer>
      <RootStackScreen isLogin={isLogin} isLoginContext={state.email} />
    </NavigationContainer>
  )
}

export default Navigation
