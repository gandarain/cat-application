const Routes = {
  ListCat: 'ListCat',
  DetailCat: 'DetailCat',
  SplashScreen: 'SplashScreen',
  Welcome: 'Welcome',
  Login: 'Login',
  Register: 'Register'
}

export default Routes
