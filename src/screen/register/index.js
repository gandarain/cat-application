import React from 'react'
import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Formik } from 'formik'

import Header from '../../component/header'
import ButtonFull from '../../component/buttonFull'

import UseRegister from './useRegister'
import validationSchema from './validationSchema'
import styles from './register.styles'

const renderErrorText = errorText => (
  <View style={styles.viewError}>
    <Text style={styles.textError}>{errorText}</Text>
  </View>
)

const renderEmail = ({ form, setForm }, handleChange, errors) => (
  <>
    <Text style={styles.textTitle}>Email</Text>
    <TextInput
      style={styles.email}
      placeholder="Alamat email kamu"
      value={form.email}
      onChangeText={value => {
        setForm({
          ...form,
          email: value
        })
        handleChange('email')
      }}
    />
    {errors.email && renderErrorText(errors.email)}
  </>
)

const renderPassword = (
  { form, setForm, showPassword, toggleShowPassword },
  handleChange,
  errors
) => (
  <>
    <Text style={styles.textTitle}>Password</Text>
    <View style={styles.containerPassword}>
      <TextInput
        style={styles.password}
        placeholder="Massukan password"
        value={form.password}
        onChangeText={value => {
          setForm({
            ...form,
            password: value
          })
          handleChange('password')
        }}
        secureTextEntry={showPassword}
      />
      <TouchableOpacity onPress={() => toggleShowPassword()}>
        <Icon
          name={showPassword ? 'eye-outline' : 'eye-off-outline'}
          size={styles.icon.size}
          color={styles.icon.color}
        />
      </TouchableOpacity>
    </View>
    {errors.password && renderErrorText(errors.password)}
  </>
)

const renderConfirmationPassword = (
  { form, setForm, toggleShowConfirmationPassword, showConfirmationPassword },
  handleChange,
  errors
) => (
  <>
    <Text style={styles.textTitle}>Konfirmasi Password</Text>
    <View style={styles.containerPassword}>
      <TextInput
        style={styles.password}
        placeholder="Massukan konfirmasi password"
        value={form.confirmation_password}
        onChangeText={value => {
          setForm({
            ...form,
            confirmation_password: value
          })
          handleChange('confirmation_password')
        }}
        secureTextEntry={showConfirmationPassword}
      />
      <TouchableOpacity onPress={() => toggleShowConfirmationPassword()}>
        <Icon
          name={showConfirmationPassword ? 'eye-outline' : 'eye-off-outline'}
          size={styles.icon.size}
          color={styles.icon.color}
        />
      </TouchableOpacity>
    </View>
    {errors.confirmation_password &&
      renderErrorText(errors.confirmation_password)}
  </>
)

const renderFooter = ({ navigateToLogin }, handleSubmit) => (
  <View>
    <ButtonFull title="Submit" onPress={() => handleSubmit()} />
    <View style={styles.footer}>
      <Text style={styles.textFooter}>Sudah punya akun?</Text>
      <Text onPress={() => navigateToLogin()} style={styles.textButton}>
        Login
      </Text>
    </View>
  </View>
)

const renderForm = useRegister => (
  <Formik
    enableReinitialize
    initialValues={{ ...useRegister.form }}
    onSubmit={() => useRegister.onSubmit()}
    validationSchema={validationSchema}
  >
    {({ handleChange, errors, handleSubmit }) => (
      <>
        <View>
          {renderEmail(useRegister, handleChange, errors)}
          {renderPassword(useRegister, handleChange, errors)}
          {renderConfirmationPassword(useRegister, handleChange, errors)}
        </View>
        {renderFooter(useRegister, handleSubmit)}
      </>
    )}
  </Formik>
)

const Register = () => {
  const useRegister = UseRegister()

  return (
    <View style={styles.container}>
      <Header title="Register" showBackButton />
      <View style={styles.content}>{renderForm(useRegister)}</View>
    </View>
  )
}

export default Register
