import { StyleSheet } from 'react-native'

import color from '../../constant/color'
import { fontSize, fontFamily, iconSize } from '../../constant/font'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.background
  },
  content: {
    flex: 1,
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginVertical: 20
  },
  textTitle: {
    color: color.font,
    fontSize: fontSize.SMALL,
    fontFamily: fontFamily.BOLD,
    marginBottom: 10
  },
  email: {
    marginBottom: 20,
    padding: 15,
    borderRadius: 6,
    fontFamily: fontFamily.REGULAR,
    fontSize: fontSize.MEDIUM,
    textAlign: 'center',
    backgroundColor: color.white
  },
  footer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  textFooter: {
    color: color.font,
    fontSize: fontSize.SMALL,
    fontFamily: fontFamily.REGULAR
  },
  textButton: {
    color: color.theme,
    fontSize: fontSize.SMALL,
    fontFamily: fontFamily.BOLD,
    marginLeft: 5
  },
  password: {
    padding: 15,
    borderRadius: 6,
    fontFamily: fontFamily.REGULAR,
    fontSize: fontSize.MEDIUM,
    textAlign: 'center',
    backgroundColor: color.white,
    width: '85%'
  },
  containerPassword: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignItems: 'center',
    marginBottom: 20
  },
  icon: {
    size: iconSize.BIG,
    color: color.font
  },
  viewError: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginVertical: 5
  },
  textError: {
    color: color.theme,
    fontSize: fontSize.SMALL,
    fontFamily: fontFamily.SEMI_BOLD
  }
})

export default styles
