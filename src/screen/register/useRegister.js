import { useState, useContext } from 'react'
import { useNavigation } from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { Context } from '../../context'
import actions from '../../context/actions'
import routes from '../../navigation/Routes'

const { SET_EMAIL } = actions

const UseRegister = () => {
  const { dispatch } = useContext(Context)
  const navigation = useNavigation()
  const [form, setForm] = useState({
    email: '',
    password: '',
    confirmation_password: ''
  })
  const [showPassword, setShowPassword] = useState(true)
  const [showConfirmationPassword, setShowConfirmationPassword] = useState(true)

  const toggleShowPassword = () => setShowPassword(!showPassword)
  const toggleShowConfirmationPassword = () =>
    setShowConfirmationPassword(!showConfirmationPassword)

  const onSubmit = async () => {
    await AsyncStorage.setItem('email', form.email)
    dispatch({
      type: SET_EMAIL,
      payload: form.email
    })
  }

  const navigateToLogin = () => navigation.navigate(routes.Login)

  return {
    navigation,
    form,
    setForm,
    showPassword,
    setShowPassword,
    toggleShowPassword,
    showConfirmationPassword,
    setShowConfirmationPassword,
    toggleShowConfirmationPassword,
    onSubmit,
    navigateToLogin
  }
}

export default UseRegister
