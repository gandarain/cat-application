import * as yup from 'yup'

const validationSchema = yup.object().shape({
  email: yup
    .string()
    .required('Email harus diisi.')
    .matches(
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      'Email tidak sesuai format.'
    ),
  password: yup
    .string()
    .required('Password harus diisi.')
    .matches(
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
      'Password tidak sesuai format.'
    ),
  confirmation_password: yup
    .string()
    .required('konfirmasi password harus diisi.')
    .oneOf([yup.ref('password'), null], 'Konfirmasi password tidak cocok.')
})

export default validationSchema
