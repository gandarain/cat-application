import React from 'react'
import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Formik } from 'formik'

import Header from '../../component/header'
import ButtonFull from '../../component/buttonFull'

import UseLogin from './useLogin'
import validationSchema from './validationSchema'
import styles from './login.styles'

const renderErrorText = errorText => (
  <View style={styles.viewError}>
    <Text style={styles.textError}>{errorText}</Text>
  </View>
)

const renderEmail = ({ form, setForm }, handleChange, errors) => (
  <>
    <Text style={styles.textTitle}>Email</Text>
    <TextInput
      style={styles.email}
      placeholder="Alamat email kamu"
      value={form.email}
      onChangeText={value => {
        setForm({
          ...form,
          email: value
        })
        handleChange('email')
      }}
    />
    {errors.email && renderErrorText(errors.email)}
  </>
)

const renderPassword = (
  { form, setForm, showPassword, toggleShowPassword },
  handleChange,
  errors
) => (
  <>
    <Text style={styles.textTitle}>Password</Text>
    <View style={styles.containerPassword}>
      <TextInput
        style={styles.password}
        placeholder="Masukan password"
        value={form.password}
        onChangeText={value => {
          setForm({
            ...form,
            password: value
          })
          handleChange('password')
        }}
        secureTextEntry={showPassword}
      />
      <TouchableOpacity onPress={() => toggleShowPassword()}>
        <Icon
          name={showPassword ? 'eye-outline' : 'eye-off-outline'}
          size={styles.icon.size}
          color={styles.icon.color}
        />
      </TouchableOpacity>
    </View>
    {errors.password && renderErrorText(errors.password)}
  </>
)

const renderFooter = ({ navigateToRegister }, handleSubmit) => (
  <View>
    <ButtonFull title="Submit" onPress={() => handleSubmit()} />
    <View style={styles.footer}>
      <Text style={styles.textFooter}>Belum punya akun?</Text>
      <Text onPress={() => navigateToRegister()} style={styles.textButton}>
        Register
      </Text>
    </View>
  </View>
)

const renderForm = useLogin => (
  <Formik
    enableReinitialize
    initialValues={{ ...useLogin.form }}
    onSubmit={() => useLogin.onSubmit()}
    validationSchema={validationSchema}
  >
    {({ handleChange, errors, handleSubmit }) => (
      <>
        <View>
          {renderEmail(useLogin, handleChange, errors)}
          {renderPassword(useLogin, handleChange, errors)}
        </View>
        {renderFooter(useLogin, handleSubmit)}
      </>
    )}
  </Formik>
)

const Login = () => {
  const useLogin = UseLogin()

  return (
    <View style={styles.container}>
      <Header title="Login" showBackButton />
      <View style={styles.content}>{renderForm(useLogin)}</View>
    </View>
  )
}

export default Login
