import { useState, useContext } from 'react'
import { useNavigation } from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { Context } from '../../context'
import actions from '../../context/actions'
import routes from '../../navigation/Routes'

const { SET_EMAIL } = actions

const UseLogin = () => {
  const { dispatch } = useContext(Context)
  const navigation = useNavigation()
  const [form, setForm] = useState({
    email: '',
    password: ''
  })
  const [showPassword, setShowPassword] = useState(true)

  const toggleShowPassword = () => setShowPassword(!showPassword)

  const onSubmit = async () => {
    await AsyncStorage.setItem('email', form.email)
    dispatch({
      type: SET_EMAIL,
      payload: form.email
    })
  }

  const navigateToRegister = () => navigation.navigate(routes.Register)

  return {
    navigation,
    form,
    setForm,
    showPassword,
    setShowPassword,
    toggleShowPassword,
    onSubmit,
    navigateToRegister
  }
}

export default UseLogin
