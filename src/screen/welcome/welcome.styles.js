import { StyleSheet } from 'react-native'

import color from '../../constant/color'
import { fontSize, fontFamily } from '../../constant/font'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.background
  },
  imageHeader: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  content: {
    height: '30%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageCat: {
    height: 100,
    width: 100
  },
  textTitle: {
    color: color.white,
    fontSize: fontSize.BIG,
    fontFamily: fontFamily.BOLD
  },
  textSubtitle: {
    color: color.font,
    fontSize: fontSize.BIG,
    fontFamily: fontFamily.REGULAR
  },
  subContent: {
    height: '23%',
    width: '100%',
    justifyContent: 'center',
    paddingHorizontal: 20
  },
  footer: {
    height: '30%',
    padding: 20,
    justifyContent: 'space-between'
  },
  textFooter: {
    color: color.font,
    fontSize: fontSize.SMALL,
    fontFamily: fontFamily.REGULAR,
    marginVertical: 10
  }
})

export default styles
