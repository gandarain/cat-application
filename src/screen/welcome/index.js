import React from 'react'
import { View, Text, ImageBackground, Image } from 'react-native'
import { useNavigation } from '@react-navigation/native'

import Header from '../../component/header'
import ButtonFull from '../../component/buttonFull'
import images from '../../asset/image'
import routes from '../../navigation/Routes'

import styles from './welcome.styles'

const { header, cat } = images

const renderContent = () => (
  <View style={styles.content}>
    <ImageBackground
      resizeMode="cover"
      source={header}
      style={styles.imageHeader}
    >
      <Image resizeMode="contain" source={cat} style={styles.imageCat} />
    </ImageBackground>
  </View>
)

const renderSubContent = () => (
  <View style={styles.subContent}>
    <Text style={styles.textFooter}>
      Lorem Ipsum is simply dummy text of the printing and typesetting industry.
      Lorem Ipsum has been the industry's standard dummy text ever
    </Text>
  </View>
)

const renderFooter = navigation => (
  <View style={styles.footer}>
    <ButtonFull
      title="Login"
      onPress={() => navigation.navigate(routes.Login)}
    />
    <Text style={styles.textFooter}>Belum punya akun?</Text>
    <ButtonFull
      title="Register"
      onPress={() => navigation.navigate(routes.Register)}
      withBorder
    />
    <Text style={styles.textFooter}>
      Dengan login dan register anda telah menyetujui syarat dan ketentuan
    </Text>
  </View>
)

const Welcome = () => {
  const navigation = useNavigation()

  return (
    <View style={styles.container}>
      <Header title="Welcome" />
      {renderContent()}
      {renderSubContent()}
      {renderFooter(navigation)}
    </View>
  )
}

export default Welcome
