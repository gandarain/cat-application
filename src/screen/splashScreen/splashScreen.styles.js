import { StyleSheet } from 'react-native'

import color from '../../constant/color'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: color.theme
  },
  imageCat: {
    height: 100,
    width: 100,
    marginTop: 10
  }
})

export default styles
