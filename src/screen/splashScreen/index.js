import React from 'react'
import { View, StatusBar, ActivityIndicator, Image } from 'react-native'

import color from '../../constant/color'
import images from '../../asset/image'

import styles from './splashScreen.styles'

const { cat } = images

const SplashScreen = () => (
  <View style={styles.container}>
    <StatusBar
      translucent
      backgroundColor={color.theme}
      barStyle="light-content"
    />
    <ActivityIndicator size="large" color={color.white} />
    <Image resizeMode="contain" source={cat} style={styles.imageCat} />
  </View>
)

export default SplashScreen
