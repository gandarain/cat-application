const SET_EMAIL = 'SET_EMAIL'

const actions = {
  SET_EMAIL
}

export default actions
