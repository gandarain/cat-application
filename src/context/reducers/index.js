import actions from '../actions'

const { SET_EMAIL } = actions

const email = (state, action) => {
  switch (action.type) {
    case SET_EMAIL:
      return { ...state, email: action.payload }
    default:
      return state
  }
}

export { email }
