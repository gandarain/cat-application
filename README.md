# Cat App
This is an application to show list of cat from this API

https://documenter.getpostman.com/view/5578104/RWgqUxxh

### How to run
- `yarn install`
- `cd ios && pod install`
- create a new file for `.env`
- copy `.env.example` to `.env`
- `yarn start`
- `yarn ios`

### Result
`iPhone 8 (13.3)`

#### Login
![login](https://github.com/gandarain/cat_app/assets/27923352/e64c86c4-3f93-4522-b38e-6aba37f7c509)

#### Register
![register](https://github.com/gandarain/cat_app/assets/27923352/2fb28142-fe32-4c22-9c5f-f3cc69885189)

#### Validation
![validation](https://github.com/gandarain/cat_app/assets/27923352/a607eec1-c585-4936-9dfa-0eab3291d352)

#### Home
![home](https://github.com/gandarain/cat_app/assets/27923352/26971a65-9f94-4714-aac5-d262ea33a9a0)