import React from 'react'
import { LogBox } from 'react-native'

import { Provider } from './src/context'
import Navigation from './src/navigation'

LogBox.ignoreLogs(['Warning: ...'])

const App = () => (
  <Provider>
    <Navigation />
  </Provider>
)

export default App
